const level = document.getElementById("LEVEL");
const alertContainer = document.getElementById("alert-container");
const alertTitle = document.getElementById("alert-title");
const infoButton = document.getElementById("info-button");

const setData = (key, data) => {
	if (key === "level") {
		setLevelValue(data);
	}
	else if (key === "btNotification")
		setBTValue(data);
};

const setLevelValue = (value) => {
	level.classList.forEach((cl) => {
		level.classList.remove(cl);
	});

	if (value < 4) 
		level.classList.add("low");
	else if (value > 13.9)
		level.classList.add("high");
	else
		level.classList.add("normal");

	level.textContent = value;
};

const setBTValue = (value) => {
	alertContainer.classList.forEach((cl) => {
		alertContainer.classList.remove(cl);
	});

	if (value == 0) {
		if (getComputedStyle(document.documentElement).getPropertyValue('--notification-opacity').trim() == '1') 
			document.documentElement.style.setProperty('--notification-opacity', '0');
		
		setTimeout(() => {
			alertTitle.textContent = "";
		}, 200);

	}
	else if (value == 1) {
		alertTitle.textContent = "Low Glucose Alert";
		alertContainer.classList.add("low-alert");
		if (getComputedStyle(document.documentElement).getPropertyValue('--notification-opacity').trim() == '0') 
			document.documentElement.style.setProperty('--notification-opacity', '1');
	}
	else if (value == 2) {
		alertTitle.textContent = "High Glucose Alert";
		alertContainer.classList.add("high-alert");
		if (getComputedStyle(document.documentElement).getPropertyValue('--notification-opacity').trim() == '0') 
			document.documentElement.style.setProperty('--notification-opacity', '1');
	}
};

// No one should use apple, so if you do, have fun accessing my website. :)))))
const apple = () => {
	let isIphone = navigator.userAgent.indexOf("iPhone") != -1;
	let isIod = navigator.userAgent.indexOf("iPod") != -1;
	let isIpad = navigator.userAgent.indexOf("iPad") != -1;
	let isMac = navigator.userAgent.indexOf("Mac") != -1;

	if (isIphone || isIod || isIpad || isMac)
		window.location = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
};

apple();

infoButton.addEventListener("mousedown", (e) => {
	console.log("Press");
	window.location = "./about/"
});